package com.mgmt.persons.service.dtos;

import java.util.Date;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class UserDataServiceOutDto {

	private Integer personId;
	private String name;
	private String lastName;
	private Date age;
	
	public UserDataServiceOutDto(Integer personId, String name, String lastName, Date age) {
		this.personId = personId;
		this.name = name;
		this.lastName = lastName;
		this.age = age;
	}
	
	public UserDataServiceOutDto() {
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getAge() {
		return age;
	}

	public void setAge(Date age) {
		this.age = age;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof UserDataServiceOutDto))
			return false;
		UserDataServiceOutDto castOther = (UserDataServiceOutDto) other;
		return new EqualsBuilder().append(personId, castOther.personId).append(name, castOther.name)
				.append(lastName, castOther.lastName).append(age, castOther.age).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(personId).append(name).append(lastName).append(age).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("personId", personId).append("name", name).append("lastName", lastName)
				.append("age", age).toString();
	}

}
