package com.mgmt.persons.service.dtos;

import java.util.Date;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CreateUserServiceInDto {

	private String name;
	private String lastName;
	private Date age;
	
	public CreateUserServiceInDto(String name, String lastName, Date age) {
		this.name = name;
		this.lastName = lastName;
		this.age = age;
	}
	
	public CreateUserServiceInDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getAge() {
		return age;
	}

	public void setAge(Date age) {
		this.age = age;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CreateUserServiceInDto))
			return false;
		CreateUserServiceInDto castOther = (CreateUserServiceInDto) other;
		return new EqualsBuilder().append(name, castOther.name)
				.append(lastName, castOther.lastName).append(age, castOther.age).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(name).append(lastName).append(age).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("name", name).append("lastName", lastName)
				.append("age", age).toString();
	}

}
