package com.mgmt.persons.service;

import java.util.Optional;

import com.mgmt.persons.models.Person;
import com.mgmt.persons.service.dtos.CreateUserServiceInDto;
import com.mgmt.persons.service.dtos.UpdateUserServiceInDto;
import com.mgmt.persons.service.dtos.UserDataServiceOutDto;

public interface PersonService {
	
	public Optional<UserDataServiceOutDto> getPersonById(final Integer id);
	
	public Person createPerson(final CreateUserServiceInDto userDto);
	
	public Person updatePerson(UpdateUserServiceInDto updatePerson, Integer id);

}
