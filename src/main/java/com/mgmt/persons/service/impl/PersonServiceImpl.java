package com.mgmt.persons.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mgmt.persons.models.Person;
import com.mgmt.persons.models.repository.PersonRepository;
import com.mgmt.persons.service.PersonService;
import com.mgmt.persons.service.dtos.CreateUserServiceInDto;
import com.mgmt.persons.service.dtos.UpdateUserServiceInDto;
import com.mgmt.persons.service.dtos.UserDataServiceOutDto;
import com.mgmt.persons.service.utils.ServiceMapperImpl;

@Service
public class PersonServiceImpl implements PersonService {

	@Autowired
	private PersonRepository repository;

	@Autowired
	private ServiceMapperImpl serviceMapper;

	@Override
	@Transactional(readOnly = true)
	public Optional<UserDataServiceOutDto> getPersonById(Integer id) {
		return serviceMapper.mapFromOptionalPersonToOptionalUsrDtaSrvOutDtoTo(
				repository.findById(id));
	}

	@Override
	@Transactional
	public Person createPerson(CreateUserServiceInDto userDto) {
				
		return repository.save(
				serviceMapper.mapFromCreateUserServiceInDtoToPerson(userDto));
	}

	@Override
	@Transactional
	public Person updatePerson(UpdateUserServiceInDto updatePerson, Integer id) {
		Optional<Person> personGot = repository.findById(id);
		
		if(Boolean.FALSE.equals(personGot.isPresent())) {
			return new Person();
		}
		
		Person person = personGot.get();
		person.setName(updatePerson.getName());
		person.setLastName(updatePerson.getLastName());
		person.setAge(updatePerson.getAge());
		
		return repository.save(person);
	}

}
