package com.mgmt.persons.service.utils;

import java.util.Optional;

import com.mgmt.persons.controller.dto.UserDataFormDto;
import com.mgmt.persons.models.Person;
import com.mgmt.persons.service.dtos.CreateUserServiceInDto;
import com.mgmt.persons.service.dtos.UpdateUserServiceInDto;
import com.mgmt.persons.service.dtos.UserDataServiceOutDto;

public interface ServiceMapper {

	public Optional<UserDataServiceOutDto> mapFromOptionalPersonToOptionalUsrDtaSrvOutDtoTo(
			final Optional<Person> from);
	
	public UserDataFormDto mapFromOptionalUserDataServiceOutDtoToUserDataFormDto(
			final Optional<UserDataServiceOutDto> from);
	
	public CreateUserServiceInDto mapFromPersonToCreateUserServiceInDto(final Person from);
	
	public Person mapFromCreateUserServiceInDtoToPerson(final CreateUserServiceInDto from);
	
	public UpdateUserServiceInDto mapFromUserDataFormDtoToUpdateUserServiceInDto(UserDataFormDto from);
	
	public Person mapFromUpdateUserServiceInDtoToPerson(final UpdateUserServiceInDto from);
	
	public UpdateUserServiceInDto mapFromPersonToUpdateUserServiceInDto(Person from);
	
}
