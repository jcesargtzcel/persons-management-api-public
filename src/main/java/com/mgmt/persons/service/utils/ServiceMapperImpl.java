package com.mgmt.persons.service.utils;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.mgmt.persons.controller.dto.UserDataFormDto;
import com.mgmt.persons.models.Person;
import com.mgmt.persons.service.dtos.CreateUserServiceInDto;
import com.mgmt.persons.service.dtos.UpdateUserServiceInDto;
import com.mgmt.persons.service.dtos.UserDataServiceOutDto;

@Component
public class ServiceMapperImpl implements ServiceMapper {

	@Override
	public Optional<UserDataServiceOutDto> mapFromOptionalPersonToOptionalUsrDtaSrvOutDtoTo(
			final Optional<Person> from) {
		UserDataServiceOutDto userDataServiceTo = new UserDataServiceOutDto();
		if (from.isPresent() && Boolean.FALSE.equals(from.get().getPersonId() == null)) {
			Person person = from.get();
			userDataServiceTo.setAge(person.getAge());
			userDataServiceTo.setLastName(person.getLastName());
			userDataServiceTo.setName(person.getName());
			userDataServiceTo.setPersonId(person.getPersonId());
		}

		return Optional.of(userDataServiceTo);

	}

	@Override
	public UserDataFormDto mapFromOptionalUserDataServiceOutDtoToUserDataFormDto(
			final Optional<UserDataServiceOutDto> from) {
		final UserDataFormDto to = new UserDataFormDto();

		if (from.isPresent()) {
			UserDataServiceOutDto userDataFrom = from.get();
			to.setPersonId(userDataFrom.getPersonId());
			to.setName(userDataFrom.getName());
			to.setLastName(userDataFrom.getLastName());
			to.setAge(userDataFrom.getAge());
		}

		return to;
	}

	@Override
	public CreateUserServiceInDto mapFromPersonToCreateUserServiceInDto(final Person from) {
		final CreateUserServiceInDto to = new CreateUserServiceInDto();
		if (Boolean.FALSE.equals(from == null)) {
			to.setName(from.getName());
			to.setLastName(from.getLastName());
			to.setAge(from.getAge());
		}

		return to;
	}

	@Override
	public Person mapFromCreateUserServiceInDtoToPerson(final CreateUserServiceInDto from) {
		final Person to = new Person();
		if (Boolean.FALSE.equals(from == null)) {
			to.setName(from.getName());
			to.setLastName(from.getLastName());
			to.setAge(from.getAge());
		}
		return to;
	}

	@Override
	public UpdateUserServiceInDto mapFromUserDataFormDtoToUpdateUserServiceInDto(UserDataFormDto from) {
		final UpdateUserServiceInDto to = new UpdateUserServiceInDto();
		if (Boolean.FALSE.equals(from == null)) {
			to.setIdPerson(from.getPersonId());
			to.setName(from.getName());
			to.setLastName(from.getLastName());
			to.setAge(from.getAge());
		}
		return to;
	}

	@Override
	public Person mapFromUpdateUserServiceInDtoToPerson(final UpdateUserServiceInDto from) {
		Person to = new Person();
		if (Boolean.FALSE.equals(from == null)) {
			to.setPersonId(from.getIdPerson());
			to.setName(from.getName());
			to.setLastName(from.getLastName());
			to.setAge(from.getAge());
		}
		return to;
	}

	@Override
	public UpdateUserServiceInDto mapFromPersonToUpdateUserServiceInDto(Person from) {
		UpdateUserServiceInDto to = new UpdateUserServiceInDto();
		if (Boolean.FALSE.equals(from == null)) {
			to.setName(from.getName());
			to.setLastName(from.getLastName());
			to.setAge(from.getAge());
		}
		return to;
	}

}
