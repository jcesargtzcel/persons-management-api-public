package com.mgmt.persons.controller.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mgmt.persons.controller.dto.ErrorDto;

@RestControllerAdvice
public class ControllerAdviceExceptions {

	@ExceptionHandler(value = RuntimeException.class)
	public ResponseEntity<ErrorDto> runtimeExceptionHandler(final RuntimeException exception){

		ErrorDto err = ErrorDto.builder().code("FMT-400")
				.message(exception.getMessage()).build();
		
		return new ResponseEntity<ErrorDto>(err, HttpStatus.BAD_REQUEST);
	}
	
	
}
