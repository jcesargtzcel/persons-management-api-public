package com.mgmt.persons.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mgmt.persons.controller.dto.UserDataFormDto;
import com.mgmt.persons.models.Person;
import com.mgmt.persons.service.PersonService;
import com.mgmt.persons.service.utils.ServiceMapperImpl;

@RestController
public class PersonController extends ValidationUtils {

	@Autowired
	private PersonService personService;

	@Autowired
	private ServiceMapperImpl serviceMapper;
	
	@Autowired
	private ValidationUtils validator;

	public static final String URL_BASE = "/public/person";

	@PostMapping(URL_BASE)
	public ResponseEntity<?> createPerson(@RequestBody final Person person) {
		validatePersonFields(person);
		
		Person createPersonResponse = new Person();
		createPersonResponse = personService.createPerson(serviceMapper.mapFromPersonToCreateUserServiceInDto(person));

		return ResponseEntity.status(HttpStatus.CREATED).body(createPersonResponse);
	}

	@GetMapping(URL_BASE + "/{idPerson}")
	public ResponseEntity<UserDataFormDto> getUserDataById(@PathVariable final Integer idPerson) {
		Optional<UserDataFormDto> person = Optional.of(serviceMapper
				.mapFromOptionalUserDataServiceOutDtoToUserDataFormDto(personService.getPersonById(idPerson)));
		if (Boolean.FALSE.equals(person.isPresent()) || person.get().getPersonId() == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(person.get());

	}

	@PutMapping(URL_BASE + "/{idPerson}")
	public ResponseEntity<?> updatePersonData(@RequestBody final Person person, final BindingResult result, @PathVariable Integer idPerson) {

		if(result.hasErrors()) {
			return validator.validate(result);
		}
		
		Person personUpdateResult = personService
				.updatePerson(serviceMapper.mapFromPersonToUpdateUserServiceInDto(person), idPerson);

		if (Boolean.FALSE.equals(Optional.of(personUpdateResult).isPresent())
				|| personUpdateResult.getPersonId() == null) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(personUpdateResult);
	}
	
}
