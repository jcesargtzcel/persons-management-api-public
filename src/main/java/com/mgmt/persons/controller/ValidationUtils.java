package com.mgmt.persons.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import com.mgmt.persons.models.Person;

@Component
public class ValidationUtils {

	public ResponseEntity<?> validate(final BindingResult result){
		Map<String, Object> errors = new HashMap<>();
		result.getFieldErrors().forEach(error -> {
			errors.put(error.getField(), error.getDefaultMessage());
		});
		
		return ResponseEntity.badRequest().body(errors);
	}
	
	public void validatePersonFields(final Person person){
		if(person.getAge() == null ||
				person.getAge().toString().isEmpty()) {
			throw new RuntimeException("Field age is required");
		}
		if(Boolean.FALSE.equals(validateDateFormat(person.getAge().toString()))) {
			throw new RuntimeException("Age must have the format 'yyy-dd-MM'");
		}
		if(person.getName() == null ||
				person.getName().toString().isEmpty()) {
			throw new RuntimeException("Field name is required");
		}
		if(person.getLastName() == null ||
				person.getLastName().toString().isEmpty()) {
			throw new RuntimeException("Field lastName is required");
		}
	}
	
	private Boolean validateDateFormat(String valiDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-dd-MM");
		dateFormat.setLenient(false);
		try {
			dateFormat.parse(valiDate);
		} catch (ParseException e) {
			return false;
		}
		return true;
	}
	
}
