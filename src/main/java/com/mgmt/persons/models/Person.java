package com.mgmt.persons.models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
@Table(name = "persons")
public class Person implements Serializable {

	private static final long serialVersionUID = -3658207159284852283L;
	
	@Id
	@Column(name = "id_person")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer personId;
	private String name;
	@Column(name = "last_name")
	private String lastName;
	@Temporal(TemporalType.DATE)
	private Date age;

	public Person() {
	}

	public Person(Integer personId, String name, String lastName, Date age) {
		this.personId = personId;
		this.name = name;
		this.lastName = lastName;
		this.age = age;
	}

	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getAge() {
		return age;
	}

	public void setAge(Date age) {
		this.age = age;
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Person))
			return false;
		Person castOther = (Person) other;
		return new EqualsBuilder().append(personId, castOther.personId).append(name, castOther.name)
				.append(lastName, castOther.lastName).append(age, castOther.age).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(personId).append(name).append(lastName).append(age).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("personId", personId).append("name", name).append("lastName", lastName)
				.append("age", age).toString();
	}

}
