package com.mgmt.persons.models.repository;

import org.springframework.data.repository.CrudRepository;

import com.mgmt.persons.models.Person;

public interface PersonRepository extends CrudRepository<Person, Integer> {
}
