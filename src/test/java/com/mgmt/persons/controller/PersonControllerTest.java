package com.mgmt.persons.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import com.mgmt.persons.service.PersonService;

@ExtendWith(MockitoExtension.class)
public class PersonControllerTest {
	
	@Autowired
	private PersonController controller;
	
	@Mock
	private PersonService personService;
	
	@Mock
	private ValidationUtils validator;
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void createPersonOkTest() {
		
	}
	
}
