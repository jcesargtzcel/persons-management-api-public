package com.mgmt.persons.controller;

import java.util.Date;

import com.mgmt.persons.models.Person;

public class PersonControllerTestFactory {

	public static Person createPerson() {
		final Person person = new Person();
		
		person.setName("Cesar");
		person.setLastName("Gutierrez");
		person.setAge(new Date(2022-05-29));
		
		return person;
	}
	
}
